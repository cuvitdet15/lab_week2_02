import 'package:flutter/material.dart';
import '../stream/word_stream.dart';

// class App extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return HomePageStream();
//   }
//
// }

class App extends StatefulWidget {


  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
  
}

class HomePageState extends State<StatefulWidget> {
  Color bgTextColor = Colors.blueGrey;
  String word = 'Welcome to Stream English Word';
  TextStream textStream = TextStream();
  late int count = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Word',

      home: Scaffold(
        appBar: AppBar(title: Text('Vocabulary'),),
        backgroundColor: bgTextColor,
        body: Center(
          child:Column(
            children:[
          Text(word, style: TextStyle(fontSize: 50)), Text(count.toString(), style: TextStyle(fontSize: 50))

          ]

          )
        ),
        floatingActionButton: FloatingActionButton(
          child: Text('C'),
          onPressed: () {
            changeWord();
          },
        ),
      ),
    );
  }

  changeWord() async {
    textStream.getWord().listen((eventEnglish) {
      setState(() {
        print(eventEnglish);
        bgTextColor = Colors.green;
       word = eventEnglish.toLowerCase();
       if (count == eventEnglish.length+1)
         count = 1;
       else count +=1;

      });
    });
  }
}

