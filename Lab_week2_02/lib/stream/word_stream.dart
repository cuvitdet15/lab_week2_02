import 'package:flutter/material.dart';

class TextStream {

  Stream<String> getWord() async* {
    final List<String> word = [
      'Bird',
      'Car',
      'Phone',
      'Table',
      'Desk'
    ];

    yield* Stream.periodic(Duration(seconds: 2), (int t) {
      int index = t % 5;

      return word[index];
    });


  }
}